import { useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Flex, Avatar, Heading, Box, ListItem } from '@chakra-ui/react';

function ContactListItem({
  contact,
  isInitiallySelected,
  handleContactItemClick,
}) {
  const checkboxRef = useRef();

  /* 
    When component mounts, set initial checked value
  */
  useEffect(() => {
    if (checkboxRef.current) {
      checkboxRef.current.checked = isInitiallySelected;
    }
  }, [isInitiallySelected]);

  const handleClick = () => {
    checkboxRef.current.checked = !checkboxRef.current.checked;
    handleContactItemClick();
  };

  const { first_name, last_name, avatar } = contact;

  return (
    <Flex
      onClick={handleClick}
      as={ListItem}
      cursor="pointer"
      w="full"
      justify="space-between"
      align="center"
    >
      <Flex>
        <Avatar name={`${first_name} ${last_name}`} src={avatar} />
        <Heading ml={2} as="h3" size="md">
          {first_name} {last_name}
        </Heading>
      </Flex>

      <Box>
        <input
          type="checkbox"
          ref={checkboxRef}
          defaultChecked={isInitiallySelected}
        />
      </Box>
    </Flex>
  );
}

ContactListItem.propTypes = {
  contact: PropTypes.object.isRequired,
  isInitiallySelected: PropTypes.bool.isRequired,
  handleContactItemClick: PropTypes.func.isRequired,
};

export default ContactListItem;
