import { useRef, useCallback } from 'react';

export default function useSelectedContactIds() {
  const selectedIdsRef = useRef([]);

  const handleContactItemClick = useCallback(
    (contactId) => () => {
      const newIds = selectedIdsRef.current.includes(contactId)
        ? selectedIdsRef.current.filter((id) => id !== contactId)
        : [...selectedIdsRef.current, contactId];

      selectedIdsRef.current = newIds;

      console.log('IDs of all selected contacts', newIds);
    },
    []
  );

  return [selectedIdsRef.current, handleContactItemClick];
}
