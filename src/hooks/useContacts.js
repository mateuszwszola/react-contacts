import { useState, useEffect } from 'react';
import fetcher from '../utils/fetcher';
import { sortContactsByLastName } from '../utils/helpers';

/*
  IDEA
  Maybe instead of showing all contacts at once, append them as the user scrolls
 */

export const CONTACTS_ENDPOINT =
  'https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json';

export default function useContacts() {
  const [contacts, setContacts] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);

  useEffect(() => {
    setIsLoading(true);

    fetcher(CONTACTS_ENDPOINT)
      .then((contacts) => {
        setContacts(sortContactsByLastName(contacts));
      })
      .catch((error) => {
        setError(error);
      })
      .finally(() => {
        setIsLoading(false);
      });
  }, []);

  return { contacts, isLoading, error };
}
