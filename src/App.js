import { useState } from 'react';
import debounce from 'lodash.debounce';
import {
  Box,
  Flex,
  Heading,
  Spinner,
  StackDivider,
  Text,
  UnorderedList,
  VStack,
} from '@chakra-ui/react';
import useContacts from './hooks/useContacts';
import useSelectedContactIds from './hooks/useSelectedIds';
import { doesContactMatchSearchTerm } from './utils/helpers';
import ContactListItem from './ContactListItem';
import SearchInput from './SearchInput';

function App() {
  const { contacts, isLoading, error } = useContacts();
  const [selectedIds, handleContactItemClick] = useSelectedContactIds();
  const [searchTerm, setSearchTerm] = useState('');

  const handleSearch = debounce((newValue) => {
    setSearchTerm(newValue);
  }, 500);

  let filteredContacts = contacts;

  if (searchTerm) {
    filteredContacts = filteredContacts.filter((contact) => {
      return doesContactMatchSearchTerm(
        searchTerm,
        `${contact.first_name} ${contact.last_name}`
      );
    });
  }

  const contactListItems = filteredContacts.map((contact) => (
    <ContactListItem
      key={contact.id}
      contact={contact}
      isInitiallySelected={selectedIds.includes(contact.id)}
      handleContactItemClick={handleContactItemClick(contact.id)}
    />
  ));

  return (
    <Box>
      <Box w="full" maxW="700px" mx="auto" py={12}>
        <Heading textAlign="center">Contacts</Heading>
        <SearchInput handleSearch={handleSearch} disabled={isLoading} />
        <Flex mt={12} w="full" justify="center" align="center">
          {error ? (
            <Text>{error.message || 'Something went wrong 😥...'}</Text>
          ) : isLoading ? (
            <Spinner />
          ) : (
            <VStack
              as={UnorderedList}
              styleType="none"
              m={0}
              p={0}
              divider={<StackDivider borderColor="gray.200" />}
              spacing={5}
              w="full"
            >
              {contactListItems}
            </VStack>
          )}
        </Flex>
      </Box>
    </Box>
  );
}

export default App;
