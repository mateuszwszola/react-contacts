import { useState } from 'react';
import PropTypes from 'prop-types';
import { Input } from '@chakra-ui/react';

function SearchInput({ handleSearch, disabled }) {
  const [searchValue, setSearchValue] = useState('');

  const handleChange = (e) => {
    const { value } = e.target;
    setSearchValue(value);
    handleSearch(value);
  };

  return (
    <Input
      isDisabled={disabled}
      mt={2}
      value={searchValue}
      onChange={handleChange}
      placeholder="Search..."
    />
  );
}

SearchInput.propTypes = {
  handleSearch: PropTypes.func.isRequired,
  disabled: PropTypes.bool.isRequired,
};

export default SearchInput;
