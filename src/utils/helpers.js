export function sortContactsByLastName(contacts) {
  return contacts.sort((a, b) => {
    if (a.last_name.toLowerCase() < b.last_name.toLowerCase()) {
      return -1;
    } else {
      return 1;
    }
  });
}

export function doesContactMatchSearchTerm(searchTerm, contactName) {
  const pattern = new RegExp(searchTerm.toLowerCase());
  return pattern.test(contactName.toLowerCase());
}
