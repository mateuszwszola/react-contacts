export default function fetcher(url) {
  return fetch(url).then(async (response) => {
    const data = await response.json();

    if (response.ok) {
      return data;
    } else {
      Promise.reject(data);
    }
  });
}
