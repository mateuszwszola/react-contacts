# React Contacts

A single webpage that displays a list of contacts.

## How to run the project locally

- Clone the repository

- Install project dependencies with `npm install`

- Run the app in the development mode with `npm start`

    - Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
